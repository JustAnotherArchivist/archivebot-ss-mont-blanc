#!/bin/bash
set -e
echo "Starting CouchDB"
/etc/init.d/couchdb start
( set +e; for i in $(seq 10); do curl -sf http://127.0.0.1:5984/ && exit 0 || sleep 1; done; echo 'CouchDB still not operational after 10 seconds'; exit 1; )
echo "Dropping privileges"
setpriv --reuid=archivebot --regid=archivebot --init-groups /home/archivebot/launch-archivebot.sh
