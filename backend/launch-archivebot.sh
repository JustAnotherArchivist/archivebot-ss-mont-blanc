#!/bin/bash
export GEM_HOME=/home/archivebot/.gems
echo "Starting Redis"
(cd /home/archivebot/redis && redis-server redis.conf) &
(cd /home/archivebot/ArchiveBot/bot && bundle exec ruby bot.rb -s "ircs://irc.hackint.org:6697" -r "redis://127.0.0.1:6379/0" -c "#notarchivebot" -n Puppeteer) &
(cd /home/archivebot/ArchiveBot && export REDIS_URL=redis://127.0.0.1:6379/0 UPDATES_CHANNEL=updates FIREHOSE_SOCKET_URL=tcp://127.0.0.1:12345 && plumbing/updates-listener | plumbing/log-firehose) &
(cd /home/archivebot/ArchiveBot && bundle exec ruby dashboard/app.rb -u http://0.0.0.0:4567) &
(cd /home/archivebot/ArchiveBot && FIREHOSE_SOCKET_URL=tcp://127.0.0.1:12345 plumbing/firehose-client | python3 dashboard/websocket.py) &
(cd /home/archivebot/ArchiveBot/cogs && bundle exec ruby start.rb) &
(cd /home/archivebot/ArchiveBot/plumbing && REDIS_URL=redis://127.0.0.1:6379/0 UPDATES_CHANNEL=updates ./analyzer) &
(cd /home/archivebot/ArchiveBot/plumbing && REDIS_URL=redis://127.0.0.1:6379/0 UPDATES_CHANNEL=updates ./trimmer >/dev/null) &
sleep inf
