Build:

    docker build -t archivebot-backend .

Run:

    docker run -d -p 80:4567 -p 4568:4568 -v abredis:/home/archivebot/redis/data/ archivebot-backend
